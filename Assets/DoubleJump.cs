﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleJump : MonoBehaviour
{
    private float Firstclicktime, Timebetweenclicks;
    private bool coroutineAllowed;
    private int clickcounter;
    Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        Firstclicktime = 0f;
        Timebetweenclicks = 0.2f;
        clickcounter = 0;
        coroutineAllowed = true;
       rb = GetComponentInChildren<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0)) 
            clickcounter += 1;
        if (clickcounter == 1 && coroutineAllowed)
        {
            Firstclicktime = Time.time;
            StartCoroutine(doubleclickdetection());
        }
        
        
    }
    private IEnumerator doubleclickdetection()
    {
        coroutineAllowed = false;
        while (Time.time <Firstclicktime + Timebetweenclicks)
        { if (clickcounter ==2)
            {
                rb.AddForce(0, 30000 * Time.deltaTime, 0);
                Debug.Log("doubleclick");
                break;
            }
            yield return new WaitForEndOfFrame();

        }

        clickcounter = 0;
        Firstclicktime = 0f;
        coroutineAllowed = true;
    }
}
