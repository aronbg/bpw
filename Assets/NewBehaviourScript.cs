﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    public float moveSpeed = 3.5f;
    private Rigidbody rbody;
    public float forward = 10f;
    bool spacebutton = false;
    bool abutton = false;
    bool dbutton = false;
    Vector3 AngleVelocity;
    

    private void Awake()
    {
        rbody = GetComponentInChildren<Rigidbody>();
        AngleVelocity = new Vector3(0, 300, 0);
    }
    private void Start()
    {
        
    }

    // Start is called before the first frame update
    public void Update()
    {

        if (Input.GetKey(KeyCode.Space))
        {
            spacebutton = true;
        }
        else { spacebutton = false; }
        if (Input.GetKey("a"))
        {
            abutton = true;
        }
        else { abutton = false; }
        if (Input.GetKey("d"))
        {
            dbutton = true;
        }
        else { dbutton = false; }


    }

    // Update is called once per frame

    private void FixedUpdate()
    {
        // rbody.AddForce(forward *Time.deltaTime, 0, 0);
        //Vector3 a = gameObject.transform.forward;
        rbody.AddForce(10f * Time.deltaTime, 0, 0);

        if (spacebutton == true)
        {
            Debug.Log("space");
            rbody.AddForce (0,800 *Time.deltaTime,0);
        }
        if (abutton == true)
        {
            Debug.Log("rotate");
           Quaternion deltarotation =Quaternion.Euler(AngleVelocity *Time.deltaTime * 4);
            rbody.MoveRotation(rbody.rotation * deltarotation);
        }
    }
 
}
